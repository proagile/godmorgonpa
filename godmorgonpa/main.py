import random


def booking_string(person_a, person_b):
    return f"- {person_a} bokar {person_b}"


assert booking_string("David", "Olof") == "- David bokar Olof"

INTRO = """
Veckans random connects! Allt är förslag.

Vill du vara med? Pinga Olof så lägger han till
dig i scriptet, som körs en gång i veckan och
postar här i Slack automagiskt.

Förslag på ämne: ställ nyfikna frågor likt PA-dags
övningen med Karin Hamrin.
Förslag på tid: 10 minuter

"""

def suggested_connects_string():
    connectors = ["Olof",
                  "David",
                  "Jesper",
                  "Mattias",
                  "HenrikB",
                  "Fredrik",
                  "HenrikZ",
                  "Patrik",
                  "Laszlo"
                  ]
    random.shuffle(connectors)
    pairs = []
    for i in range(len(connectors) // 2):
        pairs.append((connectors[i * 2], connectors[i * 2 + 1]))
    pairs = [booking_string(a, b) for (a, b) in pairs]
    result = INTRO + '\n'.join(pairs) + '\n'
    if len(connectors) % 2 == 1:
        result += f"Udda antal deltagare, så {connectors[-1]} blev över"
        result += " denna veckan. Vad ska du hitta på istället? :)"
    return result


if __name__ == '__main__':
    print(suggested_connects_string())
